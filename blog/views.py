from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import (TemplateView, ListView, DetailView, CreateView, UpdateView, DeleteView)
from blog.models import Post, Comment
from blog.forms import CommentForm
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required

# Create your views here.

class PostListView(ListView):
    model = Post
    # context_object_name = ''

class PostDetailView(DetailView):
    model = Post

# LoginRequiredMixin is used so that unauthorized user is not able to access this class based view
class CreatePostView(LoginRequiredMixin, CreateView):
    # login_url is a predefined variable, and redirects to this page if the used is not logged in
    login_url = '/admin/'
    model = Post
    # fields that is to be shown in create form for this model
    fields = ('title', 'author', 'text')

class UpdatePostView(LoginRequiredMixin, UpdateView):
    login_url = '/admin/'
    model = Post
    fields = ('title', 'author', 'text')


class DeletePostView(LoginRequiredMixin, DeleteView):
    login_url = '/admin/'
    model = Post
    success_url = reverse_lazy('post_list')


class AboutView(TemplateView):
    template_name = 'about.html'


class DraftListView(ListView):
    model = Post
    template_name = 'post_draft_list.html'

    def get_queryset(self):
        return Post.objects.filter(published_date__isnull=True).order_by('create_date')

# function view to publish post
# login_required is a decorators that ensures if the user has logged into the system
# login_url is a parameter is has which accepts url as an argument and opens that page if user is not logged in
@login_required(login_url='/admin/')
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('post_detail', pk=pk)

# add comments
@login_required(login_url='/admin/')
def add_comment_to_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        form = CommentForm()
    return render(request, 'comment_form.html', {'form': form})

@login_required(login_url='/admin/')
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    return redirect('post_detail', pk=comment.post.pk)

@login_required(login_url='/admin/')
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('post_detail', pk=post_pk)